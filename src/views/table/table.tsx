import React from 'react';
import './table.css';

interface Props {
  onSelect?: (value: number) => void;
}

export function Table({ onSelect }: Props) {
  const handleOnSelect = (value: number) => () => onSelect?.(value);
  return (
    <table className="table">
      <tbody>
        <tr>
          <td onClick={handleOnSelect(1)}>1</td>
          <td onClick={handleOnSelect(2)}>2</td>
          <td onClick={handleOnSelect(3)}>3</td>
        </tr>

        <tr>
          <td onClick={handleOnSelect(4)}>4</td>
          <td onClick={handleOnSelect(5)}>5</td>
          <td onClick={handleOnSelect(6)}>6</td>
        </tr>

        <tr>
          <td onClick={handleOnSelect(7)}>7</td>
          <td onClick={handleOnSelect(8)}>8</td>
          <td onClick={handleOnSelect(9)}>9</td>
        </tr>

        <tr>
          <td onClick={handleOnSelect(10)}>10</td>
          <td onClick={handleOnSelect(11)}>11</td>
          <td onClick={handleOnSelect(12)}>12</td>
        </tr>
      </tbody>
    </table>
  );
}
