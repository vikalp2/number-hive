import React from 'react';
import './hexagon.css';
import { ReactComponent as HexagonIcon } from './hexagon.svg';

type Color = 'white' | 'orange' | 'cyan';

interface Props {
  children: number;
  color?: Color;
}

const colors = {
  white: 'text-white',
  orange: 'text-orange-300',
  cyan: 'text-cyan-500',
};

export function Hexagon({ children: value, color = 'white' }: Props) {
  const shouldBeHidden = value === 0;

  const style = ['w-28', colors[color]].join(' ');

  return (
    <div
      style={{ visibility: shouldBeHidden ? 'hidden' : 'visible' }}
      className="relative"
    >
      <HexagonIcon className={style} />
      <label className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-2xl">{value}</label>
    </div>
  );
}
