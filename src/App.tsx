import { cloneDeep } from 'lodash';
import React, { useEffect, useState } from 'react';
import './App.css';
import { Hexagon, Table } from './views';

type Player = 1 | 2;

const playerChosenColors: Record<Player, any> = {
  1: 'orange',
  2: 'cyan',
};

type Item = {
  value: number;
  ownedBy?: Player;
};

const rows: Item[][] = [
  [{ value: 0 }, { value: 35 }, { value: 24 }, { value: 42 }, { value: 40 }],
  [{ value: 2 }, { value: 4 }, { value: 6 }, { value: 5 }, { value: 10 }],
  [
    { value: 25 },
    { value: 110 },
    { value: 49 },
    { value: 36 },
    { value: 18 },
    { value: 26 },
  ],
  [{ value: 15 }, { value: 56 }, { value: 2 }, { value: 64 }, { value: 98 }],
  [
    { value: 14 },
    { value: 28 },
    { value: 11 },
    { value: 30 },
    { value: 21 },
    { value: 3 },
  ],
  [{ value: 12 }, { value: 7 }, { value: 45 }, { value: 77 }, { value: 88 }],
  [{ value: 0 }, { value: 9 }, { value: 22 }, { value: 1 }, { value: 8 }],
];

function App() {
  const [currentTurningPlayer, setCurrentTurningPlayer] = useState<Player>(1);

  const [items, setItems] = useState<Item[][]>(rows);

  const [selectedLeft, setSelectedLeft] = useState(0);
  const [selectedRight, setSelectedRight] = useState(0);

  const handleOnSelect =
    (selectionSetter: React.Dispatch<React.SetStateAction<number>>) =>
    (value: number) => {
      selectionSetter(value);
      setCurrentTurningPlayer(currentTurningPlayer === 1 ? 2 : 1);
    };

  useEffect(() => {
    const multiply = selectedLeft * selectedRight;
    if (multiply === 0) return;

    const newItems = cloneDeep(items);

    for (let i = 0; i < newItems.length; i++) {
      const item = newItems[i];
      for (let j = 0; j < item.length; j++) {
        const label = item[j];
        if (label.value === multiply) {
          if (label.value === multiply && label.ownedBy === undefined) {
            label.ownedBy = currentTurningPlayer;
          }
        }
      }
    }

    setItems(newItems);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedLeft, selectedRight]);

  const activePlayerStyle =
    'bg-lime-200 border-lime-200 border-2 rounded-lg border-double';

  return (
    <div className="App flex flex-col w-full items-center justify-center">
      <div className="flex justify-between w-full max-w-4xl px-12 py-8">
        <span
          className={`text-4xl p-4 ${
            currentTurningPlayer === 1 ? activePlayerStyle : ''
          }`}
        >
          Sam
        </span>
        <div className="p-8"></div>
        <span
          className={`text-4xl p-4 ${
            currentTurningPlayer === 2 ? activePlayerStyle : ''
          }`}
        >
          Jess
        </span>
      </div>

      <div className="">
        {items.map((item) => (
          <div className="row" key={JSON.stringify(item)}>
            {item.map((label) => (
              <Hexagon
                key={JSON.stringify(label)}
                color={
                  label.ownedBy ? playerChosenColors[label.ownedBy] : undefined
                }
              >
                {label.value}
              </Hexagon>
            ))}
          </div>
        ))}
      </div>

      <div className="p-4"></div>

      <div className="flex items-start justify-center">
        <Table onSelect={handleOnSelect(setSelectedLeft)} />
        <div className="p-2"></div>

        <Hexagon>{selectedLeft}</Hexagon>
        <div className="p-2"></div>
        <Hexagon>{selectedRight}</Hexagon>

        <div className="p-2"></div>
        <Table onSelect={handleOnSelect(setSelectedRight)} />
      </div>
    </div>
  );
}

export default App;
